from domain.models.cart_product_model import CartProductModel


class CartProductDto:
    def __new__(cls, cart_product: dict) -> CartProductModel:
        result = CartProductModel(**cart_product)
        return result
