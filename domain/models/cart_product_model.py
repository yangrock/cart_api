from pydantic import BaseModel


class CartProductModel(BaseModel):
    id: int
    quantity: int
    unit_amount: int
    total_amount: int
    discount: int
    is_gift: bool
