from typing import List

from pydantic import BaseModel

from domain.models.cart_product_model import CartProductModel


class PayloadModel(BaseModel):
    total_amount_with_discount: int = 0
    total_discount: int = 0
    total_amount: int = 0
    products: List[CartProductModel] = []
