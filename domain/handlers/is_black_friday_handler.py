import datetime

from cross_cutting.configs.configs import BLACK_FRIDAY_YEAR, BLACK_FRIDAY_MONTH, BLACK_FRIDAY_DAY


def verify_black_friday() -> bool:
    today = datetime.date.today()
    black_friday = datetime.date(year=BLACK_FRIDAY_YEAR,
                                 month=BLACK_FRIDAY_MONTH,
                                 day=BLACK_FRIDAY_DAY)
    if today == black_friday:
        return True
    else:
        return False
