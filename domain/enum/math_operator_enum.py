from enum import Enum


class MathOperatorEnum(str, Enum):
    sum = '+'
    multiply = '*'
    subtract = '-'
