from cross_cutting.cache.cache_data import Cache
from cross_cutting.helpers.simple_math_helper import do_simple_math_operations
from domain.dtos.cart_product_dto import CartProductDto
from domain.enum.math_operator_enum import MathOperatorEnum
from domain.handlers.is_black_friday_handler import verify_black_friday
from domain.models.response_payload_model import PayloadModel
from infra.repositories.cache_repository import CacheRepository
from infra.repositories.discount_repository import DiscountRepository


class CartService:
    def __init__(self):
        self.discount_repository = DiscountRepository()
        self.cache_repository = CacheRepository(cache=Cache())

    def cart_checkout(self, request_products_list: list) -> (bool, PayloadModel) or (bool, None):
        try:
            payload_model = PayloadModel()

            products_list = self.cache_repository.get_cache_attr(cache_property='products')

            treat_black_friday(payload_model=payload_model,
                               products_list=products_list,
                               request_products_list=request_products_list)

            for item in request_products_list:
                product_id = item.get('id')
                product_quantity = item.get('quantity', 1)

                product_info = next((product for product in products_list if product.get('id') == product_id),
                                    {})

                amount = product_info.get('amount')
                is_gift = product_info.get('is_gift')

                product_total_without_discount = do_simple_math_operations(first_value=amount,
                                                                           second_value=product_quantity,
                                                                           operator=MathOperatorEnum.multiply)

                payload_model.total_amount = do_simple_math_operations(first_value=payload_model.total_amount,
                                                                       second_value=product_total_without_discount,
                                                                       operator=MathOperatorEnum.sum)

                discount = treat_discount(payload_model=payload_model,
                                          product_id=product_id,
                                          product_total=product_total_without_discount,
                                          discount_repository=self.discount_repository)

                cart_product = {
                    'id': product_id,
                    'quantity': product_quantity,
                    'unit_amount': amount,
                    'total_amount': product_total_without_discount,
                    'discount': discount,
                    'is_gift': is_gift,
                }
                payload_model.products.append(CartProductDto(cart_product=cart_product))
            return True, payload_model
        except Exception as exc:
            return False, None


def treat_black_friday(payload_model: PayloadModel,
                       products_list: list, request_products_list: list) -> None:
    is_black_friday = verify_black_friday()
    if is_black_friday:
        gift = get_gift(request_products_list=request_products_list,
                        products_list=products_list)
        if gift:
            payload_model.products.append(CartProductDto(cart_product=gift))


def get_gift(request_products_list: list, products_list: list) -> dict or None:
    gift_info = next((product for product in products_list if product.get('is_gift') is True),
                     {})
    if gift_info:
        has_gift = next((product for product in request_products_list if product.get('id') == gift_info.get('id')),
                        False)
        if not has_gift:
            gift = {
                'id': gift_info.get('id'),
                'quantity': 1,
                'unit_amount': 0,
                'total_amount': 0,
                'discount': 0,
                'is_gift': True,
            }
            return gift
    return None


def treat_discount(payload_model: PayloadModel, product_id: int, product_total: float,
                   discount_repository: DiscountRepository) -> float:
    discount = 0
    discount_percentage = discount_repository.get_discount_percentage(product_id=product_id)
    if discount_percentage:
        discount = do_simple_math_operations(first_value=product_total, second_value=discount_percentage,
                                             operator=MathOperatorEnum.multiply)

        product_total_with_discount = do_simple_math_operations(first_value=product_total, second_value=discount,
                                                                operator=MathOperatorEnum.subtract)

        payload_model.total_discount = do_simple_math_operations(first_value=payload_model.total_discount,
                                                                 second_value=discount,
                                                                 operator=MathOperatorEnum.sum)
        payload_model.total_amount_with_discount = do_simple_math_operations(
            first_value=payload_model.total_amount_with_discount,
            second_value=product_total_with_discount,
            operator=MathOperatorEnum.sum)
    return discount
