from unittest import TestCase
from unittest.mock import patch, Mock

from domain.dtos.cart_product_dto import CartProductDto
from domain.models.response_payload_model import PayloadModel
from domain.services.cart_service import CartService, treat_black_friday, treat_discount, get_gift
from tests.mocks.products_list_mock import products_list


class TestCartService(TestCase):
    @patch("domain.services.cart_service.DiscountRepository")
    @patch("domain.services.cart_service.CacheRepository")
    def setUp(self, mock_cache_repo, mock_discount_repo) -> None:
        self.mock_cache_repo = mock_cache_repo
        self.mock_discount_repo = mock_discount_repo
        self.cart_service = CartService()
        self.products_list = products_list()
        self.payload_model = PayloadModel()
        self.request_products_list = [
            {
                "id": 1,
                "quantity": 10
            }
        ]
        self.gift = {
            'id': 6,
            'quantity': 1,
            'unit_amount': 0,
            'total_amount': 0,
            'discount': 0,
            'is_gift': True,
        }

    @patch("domain.services.cart_service.treat_discount")
    def test_cart_checkout_ok(self, mock_treat_discount):
        mock_treat_discount.return_value = 757.85
        self.mock_cache_repo().get_cache_attr.return_value = self.products_list
        processed, payload = self.cart_service.cart_checkout(request_products_list=self.request_products_list)
        self.assertTrue(processed)
        self.assertIsInstance(payload, PayloadModel)

    def test_cart_checkout_return_false(self):
        self.products_list = []
        processed, payload = self.cart_service.cart_checkout(request_products_list=self.request_products_list)
        self.assertFalse(processed)
        self.assertIsNone(payload)

    @patch("domain.services.cart_service.get_gift")
    @patch("domain.services.cart_service.verify_black_friday")
    def test_treat_black_friday_when_is_black_friday(self, mock_verify_black_friday, mock_get_gift):
        mock_get_gift.return_value = self.gift
        mock_verify_black_friday.return_value = True
        treat_black_friday(payload_model=self.payload_model,
                           products_list=self.products_list,
                           request_products_list=self.request_products_list)

        self.assertIn(CartProductDto(self.gift), self.payload_model.products)

    @patch("domain.services.cart_service.verify_black_friday")
    def test_treat_black_friday_when_is_not_black_friday(self, mock_verify_black_friday):
        mock_verify_black_friday.return_value = False
        treat_black_friday(payload_model=self.payload_model,
                           products_list=self.products_list,
                           request_products_list=self.request_products_list)

        self.assertNotIn(CartProductDto(self.gift), self.payload_model.products)

    def test_get_gift_when_gift_exists(self):
        gift = get_gift(request_products_list=self.request_products_list,
                        products_list=self.products_list)
        self.assertEqual(gift, self.gift)

    def test_get_gift_when_gift_do_not_exists(self):
        list_without_gift = self.products_list.copy()
        del list_without_gift[-1]
        gift = get_gift(request_products_list=self.request_products_list,
                        products_list=list_without_gift)
        self.assertEqual(gift, None)

    def test_treat_discount(self):
        self.mock_discount_repo.get_discount_percentage = Mock(return_value=0.05)
        product_id = 1
        product_total = 15157
        discount = treat_discount(payload_model=self.payload_model,
                                  product_id=product_id,
                                  product_total=product_total,
                                  discount_repository=self.mock_discount_repo)
        self.assertEqual(discount, 757.85)
        self.assertEqual(self.payload_model.total_discount, 757.85)
