import datetime
from unittest import TestCase
from unittest.mock import patch

from domain.handlers.is_black_friday_handler import verify_black_friday


class TestIsBlackFridayHandler(TestCase):
    @patch("domain.handlers.is_black_friday_handler.BLACK_FRIDAY_YEAR", datetime.date.today().year)
    @patch("domain.handlers.is_black_friday_handler.BLACK_FRIDAY_MONTH", datetime.date.today().month)
    @patch("domain.handlers.is_black_friday_handler.BLACK_FRIDAY_DAY", datetime.date.today().day)
    def test_verify_black_friday_when_is_black_friday(self):
        is_black_friday = verify_black_friday()
        self.assertTrue(is_black_friday)

    @patch("domain.handlers.is_black_friday_handler.BLACK_FRIDAY_YEAR", datetime.date.today().year)
    @patch("domain.handlers.is_black_friday_handler.BLACK_FRIDAY_MONTH", datetime.date.today().month)
    @patch("domain.handlers.is_black_friday_handler.BLACK_FRIDAY_DAY", datetime.date.today().day + 1)
    def test_verify_black_friday_when_is_not_black_friday(self):
        is_black_friday = verify_black_friday()
        self.assertFalse(is_black_friday)
