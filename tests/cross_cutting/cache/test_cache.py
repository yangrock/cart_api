from unittest import TestCase

from cross_cutting.cache.cache_data import Cache


class TestCache(TestCase):

    def test_cache(self):
        test_case = Cache({'a': 'b', 'c': 'd', 'e': 'f'})

        self.assertNotIn('data', test_case.__dict__)

        result_cache = test_case.data
        self.assertIn('data', test_case.__dict__)

        self.assertEqual(result_cache, test_case.data)
