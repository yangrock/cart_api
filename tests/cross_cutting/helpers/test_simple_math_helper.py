from unittest import TestCase

from cross_cutting.helpers.simple_math_helper import do_simple_math_operations
from domain.enum.math_operator_enum import MathOperatorEnum


class TestSimpleMathHelper(TestCase):
    def test_simple_sum(self):
        result = do_simple_math_operations(first_value=10,
                                           second_value=10,
                                           operator=MathOperatorEnum.sum)
        self.assertEqual(result, 20)

    def test_simple_multiply(self):
        result = do_simple_math_operations(first_value=10,
                                           second_value=10,
                                           operator=MathOperatorEnum.multiply)
        self.assertEqual(result, 100)

    def test_simple_subtract(self):
        result = do_simple_math_operations(first_value=10,
                                           second_value=10,
                                           operator=MathOperatorEnum.subtract)
        self.assertEqual(result, 0)

    def test_without_valid_operator(self):
        result = do_simple_math_operations(first_value=10,
                                           second_value=10,
                                           operator='**')
        self.assertEqual(result, 0.0)
