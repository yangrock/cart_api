# Project technologies
- Python (framework: fastApi)
---
# How to start the project
Using docker you can run the following command on project root folder:
> docker compose up -d

Docker compose will start 2 pods:
- One Pod containing the CartApi
- One Pod containing the mock service to get discounts
---
# Running tests
1. Create and activate a virtualenv
>pip install virtualenv

>virtualenv venv

>venv\Scripts\activate


2. Install the coverage package with:
>pip install coverage


3. Run tests with:
>coverage run -m unittest


4. Generate the covered files with:
>coverage html


5. It'll create a folder called 'htmlvoc', inside that folder exists an "index.html" file.
6. Open it to see the coverage of the tests.
---