import json

import uvicorn
from fastapi import FastAPI

from cross_cutting.cache.cache_data import Cache
from infra.repositories.cache_repository import CacheRepository
from presentation.routers.cart_router import cart_router

app = FastAPI(title="CartAPI")

app.include_router(
    router=cart_router,
    tags=["cart"]
)


def load_products():
    with open("docs/products.json", "r") as file:
        payload = json.loads(file.read())
        cache_repository = CacheRepository(cache=Cache())
        cache_repository.upsert_cache_attr(cache_property="products", value=payload)


if __name__ == "__main__":
    load_products()
    uvicorn.run(app, host="0.0.0.0", port=8000)
