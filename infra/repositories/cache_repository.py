from typing import Optional, Any

from cross_cutting.cache.cache_data import Cache


class CacheRepository:
    def __init__(self, cache: Cache = None):
        self.cache: Optional[Cache] = cache

    def get_cache_attr(self, cache_property: str):
        return getattr(self.cache, cache_property)

    def upsert_cache_attr(self, cache_property: str, value: Any):
        setattr(self.cache, cache_property, value)

    def clear_cache_attr(self, cache_property: str):
        if hasattr(self.cache, cache_property):
            delattr(self.cache, cache_property)
            if hasattr(self.cache, f'_{cache_property}'):
                self.cache.__dict__[f'_{cache_property}'] = None
                return True
        return False
