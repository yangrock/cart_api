import grpc

from cross_cutting.configs.configs import GRPC_CONN
from infra.protos import discount_pb2, discount_pb2_grpc


class DiscountRepository:
    def __init__(self):
        self.channel = grpc.insecure_channel(GRPC_CONN)
        self.stub = discount_pb2_grpc.DiscountStub(self.channel)

    def get_discount_percentage(self, product_id: int) -> float:
        try:
            response = self.stub.GetDiscount(discount_pb2.GetDiscountRequest(productID=product_id))
            return response.percentage
        except grpc.RpcError as exc:
            status_code = exc.code()
            if grpc.StatusCode.UNAVAILABLE == status_code:
                return 0.0
            raise
