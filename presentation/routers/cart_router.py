from fastapi import APIRouter, HTTPException

from domain.models.response_payload_model import PayloadModel
from domain.services.cart_service import CartService

cart_router = APIRouter(
    prefix="/cart"
)

cart_service = CartService()


@cart_router.post("/checkout",
                  response_model=PayloadModel)
def post_cart(products_list: dict):
    if "products" in products_list:
        processed, payload = cart_service.cart_checkout(request_products_list=products_list.get("products"))
        if processed:
            return payload
        raise HTTPException(status_code=500, detail="Something went wrong processing the request!")
    else:
        raise HTTPException(status_code=400, detail="Empty Cart")
