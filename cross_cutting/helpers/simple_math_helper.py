from domain.enum.math_operator_enum import MathOperatorEnum


def do_simple_math_operations(first_value: float, second_value: float, operator: MathOperatorEnum) -> float:
    if MathOperatorEnum.multiply == operator:
        return first_value * second_value
    elif MathOperatorEnum.sum == operator:
        return first_value + second_value
    elif MathOperatorEnum.subtract == operator:
        return first_value - second_value
    else:
        return 0.0
