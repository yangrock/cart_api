import os

from dotenv import load_dotenv

load_dotenv()


BLACK_FRIDAY_DAY = int(os.getenv("BLACK_FRIDAY_DAY", 25))
BLACK_FRIDAY_MONTH = int(os.getenv("BLACK_FRIDAY_MONTH", 10))
BLACK_FRIDAY_YEAR = int(os.getenv("BLACK_FRIDAY_YEAR", 2021))
GRPC_CONN = os.getenv("GRPC_CONN")
