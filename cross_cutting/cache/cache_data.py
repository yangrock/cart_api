from functools import cached_property
from typing import Any, Optional

from cross_cutting.helpers.singleton_helper import SingletonMeta


class Cache(metaclass=SingletonMeta):

    def __init__(self, data: Optional[Any] = None):
        self._data = data

    @cached_property
    def data(self):
        return self._data
